/**
 *    __  ___         __     ______                 ____  
 *   /  |/  /__ _____/ /__  /_  __/_ _____________ / / /  
 *  / /|_/ / _ `/ __/  '_/   / / / // / __/ __/ -_) / /   
 * /_/  /_/\_,_/_/ /_/\_\   /_/  \_, /_/ /_/  \__/_/_/    
 *                              /___/
 * ObjectTypeController
 * Controller class for objectType component.
 * Copyright (c) 2017, Mark Tyrrell
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
public class ObjectTypeController
{
    private static final String ERROR_OBJECT_NOT_FOUND = 'Object not found';
    private static final String ERROR_FIELD_NOT_FOUND = 'Field not found';
    private static final String ERROR_FIELDSET_NOT_FOUND = 'Fieldset not found';

    ////////////////////////////////////////////////////////////////////////////
    // PRIVATE METHODS
    ////////////////////////////////////////////////////////////////////////////
    
    @TestVisible
    private static Map<String, ObjectField> getObjectFieldMap(Schema.DescribeSObjectResult objectDescribe, List<String> fields)
    {
        Map<String, ObjectField> objectFieldMap = new Map<String, ObjectField>();
        Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();

        for (String field : fields)
        {
            if (fieldMap.containsKey(field))
            {
                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(field).getDescribe();
                objectFieldMap.put(field, new ObjectField(fieldDescribe));
            }
            else
            {
                throw new AuraHandledException(ERROR_FIELD_NOT_FOUND + ': ' + field);
            }
        }

        return objectFieldMap;
    }

    @TestVisible
    private static Map<String, List<ObjectField>> getObjectFieldSetMap(Schema.DescribeSObjectResult objectDescribe, List<String> fieldSets)
    {
        Map<String, List<ObjectField>> objectFieldSetMap = new Map<String, List<ObjectField>>();
        Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();
        Map<String, Schema.FieldSet> fieldSetMap = objectDescribe.fieldSets.getMap();

        for (String fieldSet : fieldSets)
        {
            if (fieldSetMap.containsKey(fieldSet))
            {
                List<Schema.FieldSetMember> fieldSetMembers =  fieldSetMap.get(fieldSet).getFields();
                List<ObjectField> fieldList = new List<ObjectField>();

                for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
                {
                    Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldSetMember.getFieldPath()).getDescribe();
                    fieldList.add(new ObjectField(fieldDescribe));
                }

                objectFieldSetMap.put(fieldSet, fieldList);
            }
            else
            {
                throw new AuraHandledException(ERROR_FIELDSET_NOT_FOUND + ': ' + fieldSet);
            }
        }

        return objectFieldSetMap;
    }

    @TestVisible
    private static List<PicklistEntry> getPicklistEntryList(Schema.DescribeFieldResult fieldDescribe)
    {
        List<PicklistEntry> picklistEntryList = new List<PicklistEntry>();
        List<Schema.PicklistEntry> picklistValues = fieldDescribe.getPicklistValues();

        for (Schema.PicklistEntry picklistEntry : picklistValues)
        {
            picklistEntryList.add(new PicklistEntry(picklistEntry));
        }

        return picklistEntryList;
    }

    @TestVisible
    private static List<String> getReferenceToList(Schema.DescribeFieldResult fieldDescribe)
    {
        List<String> referenceToList = new List<String>();
        List<Schema.SObjectType> referenceToObjects = fieldDescribe.getReferenceTo();

        for (Schema.SObjectType objectType : referenceToObjects)
        {
            referenceToList.add(objectType.getDescribe().getName());
        }

        return referenceToList;
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC METHODS
    ////////////////////////////////////////////////////////////////////////////

    @AuraEnabled
    public static ObjectType getObjectType(String objectName, String fieldsJSON, String fieldSetsJSON)
    {
        List<String> fields = (List<String>) JSON.deserialize(fieldsJSON, List<String>.class);
        List<String> fieldSets = (List<String>) JSON.deserialize(fieldSetsJSON, List<String>.class);
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

        if (globalDescribe.containsKey(objectName))
        {
            Schema.DescribeSObjectResult objectDescribe = globalDescribe.get(objectName).getDescribe();
            ObjectType objectType = new ObjectType(objectDescribe);

            if (fields != null)
            {
                objectType.fields = getObjectFieldMap(objectDescribe, fields);
            }

            if (fieldSets != null)
            {
                objectType.fieldSets = getObjectFieldSetMap(objectDescribe, fieldSets);
            }

            return objectType;
        }
        else
        {
            throw new AuraHandledException(ERROR_OBJECT_NOT_FOUND + ': ' + objectName);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC CLASSES
    ////////////////////////////////////////////////////////////////////////////

    public class ObjectType
    {
        @AuraEnabled public Map<String, ObjectField> fields;
        @AuraEnabled public Map<String, List<ObjectField>> fieldSets;
        @AuraEnabled public String keyPrefix;
        @AuraEnabled public String label;
        @AuraEnabled public String labelPlural;
        @AuraEnabled public String name;
        @AuraEnabled public Boolean accessible;
        @AuraEnabled public Boolean createable;
        @AuraEnabled public Boolean custom;
        @AuraEnabled public Boolean deletable;
        @AuraEnabled public Boolean mergeable;
        @AuraEnabled public Boolean queryable;
        @AuraEnabled public Boolean searchable;
        @AuraEnabled public Boolean undeletable;
        @AuraEnabled public Boolean updateable;

        public ObjectType(Schema.DescribeSObjectResult objectDescribe)
        {
            this.fields = new Map<String, ObjectField>();
            this.fieldSets = new Map<String, List<ObjectField>>();
            this.keyPrefix = objectDescribe.getKeyPrefix();
            this.label = objectDescribe.getLabel();
            this.labelPlural = objectDescribe.getLabelPlural();
            this.name = objectDescribe.getName();
            this.accessible = objectDescribe.isAccessible();
            this.createable = objectDescribe.isCreateable();
            this.custom = objectDescribe.isCustom();
            this.deletable = objectDescribe.isDeletable();
            this.mergeable = objectDescribe.isMergeable();
            this.queryable = objectDescribe.isQueryable();
            this.searchable = objectDescribe.isSearchable();
            this.undeletable = objectDescribe.isUndeletable();
            this.updateable = objectDescribe.isUpdateable();
        }
    }

    public class ObjectField
    {
        @AuraEnabled public Integer byteLength;
        @AuraEnabled public String calculatedFormula;
        @AuraEnabled public String controller;
        @AuraEnabled public String defaultValueFormula;
        @AuraEnabled public Integer digits;
        @AuraEnabled public String inlineHelpText;
        @AuraEnabled public String label;
        @AuraEnabled public Integer length;
        @AuraEnabled public String localName;
        @AuraEnabled public String name;
        @AuraEnabled public List<PicklistEntry> picklistValues;
        @AuraEnabled public Integer precision;
        @AuraEnabled public List<String> referenceTo;
        @AuraEnabled public String relationshipName;
        @AuraEnabled public Integer relationshipOrder;
        @AuraEnabled public Integer scale;
        @AuraEnabled public String soapType;
        @AuraEnabled public String sObjectField;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean accessible;
        @AuraEnabled public Boolean autoNumber;
        @AuraEnabled public Boolean calculated;
        @AuraEnabled public Boolean cascadeDelete;
        @AuraEnabled public Boolean caseSensitive;
        @AuraEnabled public Boolean createable;
        @AuraEnabled public Boolean custom;
        @AuraEnabled public Boolean defaultedOnCreate;
        @AuraEnabled public Boolean dependentPicklist;
        @AuraEnabled public Boolean externalId;
        @AuraEnabled public Boolean filterable;
        @AuraEnabled public Boolean groupable;
        @AuraEnabled public Boolean htmlFormatted;
        @AuraEnabled public Boolean idLookup;
        @AuraEnabled public Boolean nameField;
        @AuraEnabled public Boolean namePointing;
        @AuraEnabled public Boolean nillable;
        @AuraEnabled public Boolean permissionable;
        @AuraEnabled public Boolean restrictedDelete;
        @AuraEnabled public Boolean restrictedPicklist;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public Boolean unique;
        @AuraEnabled public Boolean updateable;
        @AuraEnabled public Boolean writeRequiresMasterRead;

        public ObjectField(Schema.DescribeFieldResult fieldDescribe)
        {
            this.byteLength = fieldDescribe.getByteLength();
            this.calculatedFormula = fieldDescribe.getCalculatedFormula();
            this.controller = fieldDescribe.getController() != null ? fieldDescribe.getController().getDescribe().getName() : null;
            this.defaultValueFormula = (String) fieldDescribe.getDefaultValue();
            this.digits = fieldDescribe.getDigits();
            this.inlineHelpText = fieldDescribe.getInlineHelpText();
            this.label = fieldDescribe.getLabel();
            this.length = fieldDescribe.getLength();
            this.localName = fieldDescribe.getLocalName();
            this.name = fieldDescribe.getName();
            this.picklistValues = getPicklistEntryList(fieldDescribe);
            this.precision = fieldDescribe.getPrecision();
            this.referenceTo = getReferenceToList(fieldDescribe);
            this.relationshipName = fieldDescribe.getRelationshipName();
            this.relationshipOrder = fieldDescribe.getRelationshipOrder();
            this.scale = fieldDescribe.getScale();
            this.soapType = fieldDescribe.getSoapType().name();
            this.sObjectField = fieldDescribe.getSobjectField().getDescribe().getName();
            this.type = fieldDescribe.getType().name();
            this.accessible = fieldDescribe.isAccessible();
            this.autoNumber = fieldDescribe.isAutoNumber();
            this.calculated = fieldDescribe.isCalculated();
            this.cascadeDelete = fieldDescribe.isCascadeDelete();
            this.caseSensitive = fieldDescribe.isCaseSensitive();
            this.createable = fieldDescribe.isCreateable();
            this.custom = fieldDescribe.isCustom();
            this.defaultedOnCreate = fieldDescribe.isDefaultedOnCreate();
            this.dependentPicklist = fieldDescribe.isDependentPicklist();
            this.externalId = fieldDescribe.isExternalId();
            this.filterable = fieldDescribe.isFilterable();
            this.groupable = fieldDescribe.isGroupable();
            this.htmlFormatted = fieldDescribe.isHtmlFormatted();
            this.idLookup = fieldDescribe.isIdLookup();
            this.nameField = fieldDescribe.isNameField();
            this.namePointing = fieldDescribe.isNamePointing();
            this.nillable = fieldDescribe.isNillable();
            this.permissionable = fieldDescribe.isPermissionable();
            this.restrictedDelete = fieldDescribe.isRestrictedDelete();
            this.restrictedPicklist = fieldDescribe.isRestrictedPicklist();
            this.sortable = fieldDescribe.isSortable();
            this.unique = fieldDescribe.isUnique();
            this.updateable = fieldDescribe.isUpdateable();
            this.writeRequiresMasterRead = fieldDescribe.isWriteRequiresMasterRead();
        }
    }

    public class PicklistEntry
    {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Boolean active;
        @AuraEnabled public Boolean defaultValue;

        public PicklistEntry(Schema.PicklistEntry picklistEntry)
        {
            this.label = picklistEntry.getLabel();
            this.value = picklistEntry.getValue();
            this.active = picklistEntry.isActive();
            this.defaultValue = picklistEntry.isDefaultValue();
        }
    }
}