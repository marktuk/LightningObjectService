/**
 *    __  ___         __     ______                 ____  
 *   /  |/  /__ _____/ /__  /_  __/_ _____________ / / /  
 *  / /|_/ / _ `/ __/  '_/   / / / // / __/ __/ -_) / /   
 * /_/  /_/\_,_/_/ /_/\_\   /_/  \_, /_/ /_/  \__/_/_/    
 *                              /___/
 * ObjectTypeController_Test
 * Test class for ObjectTypeController class.
 * Copyright (c) 2017, Mark Tyrrell
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
@isTest
public class ObjectTypeController_Test
{
    @isTest
    private static void test_getObjectFieldMap()
    {
        // setup data
        Schema.DescribeSObjectResult objectDescribe = Schema.sObjectType.Account;
        List<String> fields = new List<String>{ 'Name', 'Description' };
    
        // run test
        Test.startTest();
            Map<String, ObjectTypeController.ObjectField> objectFieldMap = ObjectTypeController.getObjectFieldMap(objectDescribe, fields);
        Test.stopTest();
    
        // assert results
        System.assert(objectFieldMap.containsKey('Name'));
        System.assert(objectFieldMap.containsKey('Description'));
        System.assertNotEquals(null, objectFieldMap.get('Name'));
        System.assertNotEquals(null, objectFieldMap.get('Description'));
    }

    @isTest
    private static void test_getObjectFieldSetMap()
    {
        // setup data
        // NOTE: create a fieldset on Account or pick an object that has
        // fieldsets to improve code coverage.
        Schema.DescribeSObjectResult objectDescribe = Schema.sObjectType.Account;
        Map<String, Schema.FieldSet> fieldSetMap = objectDescribe.fieldSets.getMap();
        List<String> fieldSets = new List<String>();

        // add a fieldset, if one exists
        for (String fieldSet : fieldSetMap.keySet())
        {
            fieldSets.add(fieldSet);
            break;
        }
    
        // run test
        Test.startTest();
            Map<String, List<ObjectTypeController.ObjectField>> objectFieldSetMap = ObjectTypeController.getObjectFieldSetMap(objectDescribe, fieldSets);
        Test.stopTest();
    
        // assert results
        System.assertNotEquals(null, objectFieldSetMap);

        for (String fieldSet : fieldSets)
        {
            System.assert(objectFieldSetMap.containsKey(fieldSet));
            System.assertNotEquals(null, objectFieldSetMap.get(fieldSet));
        }
    }

    @isTest
    private static void test_getPicklistEntryList()
    {
        // setup data
        Schema.DescribeFieldResult fieldDescribe = Schema.sObjectType.Account.fields.Type;
        List<Schema.PicklistEntry> picklistValues = fieldDescribe.getPicklistValues();
    
        // run test
        Test.startTest();
            List<ObjectTypeController.PickListEntry> picklistEntryList = ObjectTypeController.getPicklistEntryList(fieldDescribe);
        Test.stopTest();
    
        // assert results
        System.assertEquals(picklistValues.size(), picklistEntryList.size());
        System.assertEquals(picklistValues[0].getValue(), picklistEntryList[0].value);
    }

    @isTest
    private static void test_getReferenceToList()
    {
        // setup data
        Schema.DescribeFieldResult fieldDescribe = Schema.sObjectType.Contact.fields.AccountId;
        List<Schema.SObjectType> referenceTo = fieldDescribe.getReferenceTo();
        
        // run test
        Test.startTest();
            List<String> referenceToList = ObjectTypeController.getReferenceToList(fieldDescribe);
        Test.stopTest();
    
        // assert results
        System.assertEquals(referenceTo.size(), referenceToList.size());
        System.assertEquals(referenceTo[0].getDescribe().getName(), referenceToList[0]);
    }

    @isTest
    private static void test_getObjectType()
    {
        // setup data
        Schema.DescribeSObjectResult objectDescribe = Schema.sObjectType.Account;
        String objectName = Schema.sObjectType.Account.getName();
        List<String> fields = new List<String>{ 'Name', 'Description' };
        Map<String, Schema.FieldSet> fieldSetMap = objectDescribe.fieldSets.getMap();
        List<String> fieldSets = new List<String>();

        // add a fieldset, if one exists
        for (String fieldSet : fieldSetMap.keySet())
        {
            fieldSets.add(fieldSet);
            break;
        }

        String fieldsJSON = JSON.serialize(fields);
        String fieldSetJSON = JSON.serialize(fieldSets);
    
        // run test
        Test.startTest();
            ObjectTypeController.ObjectType objectType = ObjectTypeController.getObjectType(objectName, fieldsJSON, fieldSetJSON);
        Test.stopTest();
    
        // assert results
        System.assertNotEquals(null, objectType);
        System.assertEquals(fields.size(), objectType.fields.size());
    }
}