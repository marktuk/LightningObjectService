/**
 *    __  ___         __     ______                 ____  
 *   /  |/  /__ _____/ /__  /_  __/_ _____________ / / /  
 *  / /|_/ / _ `/ __/  '_/   / / / // / __/ __/ -_) / /   
 * /_/  /_/\_,_/_/ /_/\_\   /_/  \_, /_/ /_/  \__/_/_/    
 *                              /___/
 * objectTypeHelper.js
 * Copyright (c) 2017, Mark Tyrrell
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
({
    getObjectType: function(component)
    {
        var action = component.get('c.getObjectType');
        var callback = component.get('v.onLoad');

        action.setParams({
            objectName: component.get('v.sObjectName'),
            fieldsJSON: JSON.stringify(component.get('v.fields')),
            fieldSetsJSON: JSON.stringify(component.get('v.fieldSets'))
        });

        action.setCallback(this, function(response)
            {
                var state = response.getState();

                if (state === 'SUCCESS')
                {
                    var returnValue = response.getReturnValue();
                    component.set('v.assignTo', returnValue);
                }
                else if (state === 'INCOMPLETE')
                {
                    component.set('v.error', {
                        message: 'Server could not be reached. Check your internet connection.'
                    });
                }
                else
                {
                    var error  = response.getError();
                    throw new Error(error[0].message);
                }

                if (callback) $A.enqueueAction(callback);
            });

        action.setStorable();

        $A.enqueueAction(action);
    }
})