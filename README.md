> **IMPORTANT** - This project has been **archived** as the functionality it provides is now available using the [`lightning/uiObjectInfoApi`](https://developer.salesforce.com/docs/component-library/bundle/lightning-ui-object-info-api/documentation) module provided as part of [Lightning Web Components](https://developer.salesforce.com/docs/component-library/documentation/lwc).

# Lightning Object Service

The Lightning Object Service is a custom lightning service component designed to replicate the functionality provided by the [`$ObjectType`](https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_variables_global_objecttype.htm) global variable only available in Visualforce. The object returned replicates the Visualforce `$ObjectType` global variable as closely as possible, with a few minor tweaks. This should make refactoring simpler if/when Salesforce make `$ObjectType` available in Lightning as a gloabl value provider.

## Usage
Simply instantiate the component within your component markup specifying which object, fields & fieldsets you need schema details for, and the results will be assigned to the specified attribute on init. 

```HTML
<c:objectType aura:id="objectType"
    sObjectName="{!v.sObjectName}"
    fields="{!v.fields}"
    fieldSets="{!v.fieldSets}"
    assignTo="{!v.objectType}"
    onLoad="{!c.objectTypeLoaded}" />
```

##### Methods
This component supports the following methods.
- `reload`: Performs the same load function as on init using the current configuration values. Doesn’t force a server trip unless required.

### Attributes

| Attribute Name | Attribute Type | Description | Required? |
|----------------|----------------|-------------|-----------|
| body | Component[] | The body of the component. In markup, this is everything in the body of the tag. |  |
| sObjectName | String | The SObject API name. | Yes |
| fields | String[] | Specifies which of the SObjectType's fields to include in the returned describe information. |  |
| fieldSets | String[] | Specifies which of the SObjectType's fieldsets to include in the returned describe information. |  |
| assignTo | Object | The returned SObjectType describe information will be assigned to this attribute. | Yes |
| error | Object | Will contain an error object if there was a problem retrieving the describe information. |  |
| onLoad | Aura.Action | An action called on successful/unsuccessful load of the describe information. |  |

## Installation
The simplest way to install the component is to use either the [Salesforce DX CLI](https://developer.salesforce.com/tools/sfdxcli) or the [Force.com CLI](https://github.com/heroku/force) to deploy the metadata to a sandbox.

###### Salesforce DX CLI
```bash
$ sfdx force:auth:web:login
$ sfdx force:mdapi:deploy -d src -u <username>
```

###### Force.com CLI
```bash
$ force login -i=login  
$ force import -directory=src
```

## Tests
Unit tests are included for the server-side Apex controller. These tests will provide 100% coverage in most orgs providing there's atleast one fieldset on the standard Account object. The tests have been written to work in any org regardless of custom configuration, feel free to tailor the unit tests to your specific org configuration.

## License
This project is licensed under the terms of the BSD license.